// Import JavaScript modules
import { registerSettings } from './scripts/settings.js';
import { preloadTemplates } from './scripts/preloadTemplates.js';
import { importCharacterData, importItemData, setDriverID } from './scripts/mapDataFunctions.js';
import { fetchSavagedusData } from './scripts/api.js';
import { getSelectedOptions, sortCharacters, createImportFolder, createImportedFolderName } from './scripts/utils.js';
import SavagedImport from './scripts/SavagedImport.js';

//CONFIG.debug.hooks = true

export const MODULE_TITLE = 'Savaged.us Importer';
export const MODULE_ID = 'savagedus-importer';

/* ------------------------------------ */
/* Initialize module					*/
/* ------------------------------------ */
Hooks.once('init', async function () {
    // Assign custom classes and constants here
    // Register custom module settings
    registerSettings();
    // Preload Handlebars templates
    await preloadTemplates();
    // Register custom sheets (if any)
});

/* ------------------------------------ */
/* Setup module							*/
/* ------------------------------------ */
Hooks.once('setup', function () {

});

/* ------------------------------------ */
/* When ready							*/
/* ------------------------------------ */
Hooks.once('ready', function () {
    // Do anything once the module is ready
});

// characters: incoming import data

Hooks.on('renderActorDirectory', (app, html, data) => {

    if (game.users.get(game.userId).role > 2) {
        const patchAllActorsAvailable = typeof patchActorsDialog !== 'undefined';
        const patchActors = patchAllActorsAvailable ? game.settings.get('savagedus-importer', 'patchActors') : false;
        const executePatchActors = patchAllActorsAvailable && patchActors;
        const apiKey = game.settings.get(MODULE_ID, "apiKey");
        const footer = document.createDocumentFragment();
        const headerText = document.createTextNode(MODULE_TITLE);
        const headerTitle = document.createElement('h1');
        headerTitle.className = 'savagedus-title';

        headerTitle.appendChild(headerText);

        footer.append(headerTitle);

        const buttonGrid = document.createElement('div');
        buttonGrid.className = 'button-grid';

        const fileImportButton = document.createElement('button');
        fileImportButton.innerHTML = `<i class="fas fa-file-import"></i>Files`;
        fileImportButton.className = 'savagedus-file-import';
        fileImportButton.title = 'Import generic JSON files.';

        buttonGrid.append(footer, fileImportButton);
        if (apiKey) {
            const characterImportButton = document.createElement('button');
            characterImportButton.innerHTML = `<i class="fas fa-cloud-download-alt"></i>Characters`;
            characterImportButton.className = 'savagedus-character-import';
            characterImportButton.title = 'Import saves from Savaged.us as Characters or NPCs.';
            const refreshCacheButton = document.createElement('button');
            refreshCacheButton.innerHTML = `<i class="fas fa-sync-alt"></i>Refresh Cache`;
            refreshCacheButton.className = 'savagedus-cache-refresh';
            refreshCacheButton.title = 'If data is missing or images aren\'t loading, try regenerating the cache on Savaged.us.';
            const bestiaryImportButton = document.createElement('button');
            bestiaryImportButton.innerHTML = `<i class="fas fa-cloud-download-alt"></i>Bestiary`;
            bestiaryImportButton.className = 'savagedus-bestiary-import';
            bestiaryImportButton.title = 'Search and import bestiary entries from Savaged.us.';
            buttonGrid.append(bestiaryImportButton, characterImportButton, refreshCacheButton);

            // Button for API Import handling
            characterImportButton.addEventListener('click', async ev => {
                importCharacter(apiKey, 'character', executePatchActors);
            });

            refreshCacheButton.addEventListener('click', async ev => {
                refreshCacheButton.innerHTML = `<i class="fas fa-spinner fa-spin"></i>Refresh Cache`;
                const regeneratedCache = await fetchSavagedusData(apiKey, 'cachebuster');
                refreshCacheButton.innerHTML = `<i class="fas fa-sync-alt"></i>Refresh Cache`;
                if (regeneratedCache) {
                    ui.notifications.info("Savaged.us Importer: Successfully regenerated character saves cache.");
                } else {
                    ui.notifications.error("Savaged.us Importer: Character saves cache regeneration failed.");
                }
            });

            bestiaryImportButton.addEventListener('click', async ev => {
                let selectedResults = [];
                let books;
                let content = await renderTemplate("modules/savagedus-importer/templates/bestiary-search.hbs", books);

                let savagedInPrompt = new SavagedImport({
                    title: `${MODULE_TITLE} - Search the Bestiary`,
                    content: content,
                    buttons: {
                        "Import": {
                            icon: `<i class="fas fa-cloud-download-alt"></i>`,
                            label: "Import",
                            callback: async () => {
                                selectedResults = getSelectedOptions(document.getElementById('savagedus-bestiary-results-list'));
                                // Create new import folder
                                let newFolder = await createImportFolder('Actor');
                                for (const selectedResult of selectedResults) {
                                    let resultData = SavagedImport.results.find(result => result.id == selectedResult);
                                    let importedActor = await importCharacterData(resultData, null, { method: 'API', folder: newFolder }, 'npc');
                                    importedActor.folder = newFolder.id;
                                    const actor = await Actor.create(importedActor);
                                    // Add potential AEs
                                    appendActiveEffects(actor, resultData);
                                }

                                if (executePatchActors) {
                                    patchActorsDialog();
                                }
                            }
                        },
                        "Cancel": {
                            icon: `<i class="fas fa-window-close"></i>`,
                            label: "Cancel",
                            callback: () => { }
                        }
                    },
                    render: async html => {
                        styleAppWindowDialog(html);
                    },
                    close: async () => { }
                }, { popout: true, width: 540, resizable: false });
                savagedInPrompt.render(true);

                // Now fetch the books
                books = await fetchSavagedusData(apiKey, 'books');
                books.unshift({ name: 'Custom Entries', id: 0 });
                if (books) {
                    content = await renderTemplate("modules/savagedus-importer/templates/bestiary-search.hbs", books);
                }

                savagedInPrompt.data.content = content;

                savagedInPrompt.render(true);
            });
        }

        fileImportButton.addEventListener('click', async ev => {
            const content = await renderTemplate('modules/savagedus-importer/templates/file-import.hbs');

            let savagedInPrompt = new Dialog({
                title: `${MODULE_TITLE} - Import Generic JSON Files`,
                content: content,
                buttons: {
                    "Import": {
                        icon: `<i class="fas fa-file-import"></i>`,
                        label: "Import",
                        callback: async (e) => {
                            const files = savagedInPrompt.data.files;
                            const fileLabels = document.getElementsByClassName('savagedus-file-import-label');
                            const types = {};

                            for (const fileLabel of fileLabels) {
                                if (fileLabel.nextElementSibling.value === 'character') {
                                    types[fileLabel.dataset.uuid] = 'character';
                                } else {
                                    types[fileLabel.dataset.uuid] = 'npc';
                                }
                            }

                            if (files) {
                                // Create Folder for new imports and empty types object
                                let folderName = await createImportedFolderName(),
                                    folder = null;

                                // Capture types for each uuid
                                for (const file of files) {
                                    const json = await readTextFromFile(file);
                                    const characterData = JSON.parse(json);
                                    const uuid = characterData.uuid;
                                    const inType = types[uuid];
                                    const existingActor = game.actors.find((a) => a.getFlag(MODULE_ID, 'uuid') === uuid);
                                    let confirmationMessage = '';

                                    if (existingActor) {
                                        // update
                                        let subfolder = existingActor.folder;

                                        // Create import folder for new allies and vehicles
                                        if (subfolder === null && (characterData.alliedExtras.length || characterData.vehicles.length)) {
                                            subfolder = await createImportFolder('Actor', folderName);
                                        }

                                        const actorData = await importCharacterData(characterData, existingActor, { method: 'File', folder: subfolder, itemsFolderName: folderName }, inType);
                                        await existingActor.update(actorData);
                                        // Add potential AEs
                                        appendActiveEffects(existingActor, characterData);

                                        confirmationMessage = `<p>@Actor[${existingActor.name}] has been updated.</p>`;
                                    } else {
                                        // new
                                        // Create import folder
                                        if (folder === null) {
                                            folder = await createImportFolder('Actor', folderName);
                                        }

                                        const actorData = await importCharacterData(characterData, null, { method: 'API', folder: folder, itemsFolderName: folderName }, inType);
                                        actorData.folder = folder.id;
                                        const actor = await Actor.create(actorData);
                                        // Add potential AEs
                                        appendActiveEffects(actor, characterData);

                                        confirmationMessage = `<p>@Actor[${actor.name}] has been imported.</p>`;
                                    }

                                    // Set as driver of vehicles
                                    if (typeof characterData.vehicles !== 'undefined' && characterData.vehicles.length) {
                                        await setDriverID(actor);
                                    }

                                    ChatMessage.create({ content: confirmationMessage });
                                }

                                if (executePatchActors) {
                                    patchActorsDialog();
                                }
                            } else {
                                ui.notifications.warn("Savaged.us Importer: No files selected.");
                            }
                        }
                    },
                    "Cancel": {
                        icon: `<i class="fas fa-window-close"></i>`,
                        label: "Cancel",
                        callback: () => { }
                    }
                },
                render: async html => {
                    styleAppWindowDialog(html);
                    const selectedFiles = savagedInPrompt.options.files;
                    let fileInput = document.querySelector('#savaged-json-files');
                    if (selectedFiles) {
                        fileInput = selectedFiles;
                    }

                    fileInput.addEventListener('change', async (event) => {
                        const files = event.target.files;
                        const characters = [];
                        for (const file of files) {
                            const json = await readTextFromFile(file);
                            const stringData = JSON.parse(json);
                            characters.push(stringData);
                        }
                        const content = await renderTemplate("modules/savagedus-importer/templates/file-import.hbs", { files: files, characters: characters });
                        savagedInPrompt.data.content = content;
                        savagedInPrompt.data.files = files;
                        await savagedInPrompt.render(true);
                    });

                    html[0].parentElement.parentElement.style.height = 'auto';
                },
                close: async () => { }
            });
            savagedInPrompt.render(true);
        });
        //TODO: default buttons so people can hit enter to import or search
        html.find('.directory-footer').append(buttonGrid);
    }
});

Hooks.on('renderItemDirectory', (app, html, data) => {
    if (game.users.get(game.userId).role > 2) {
        const apiKey = game.settings.get(MODULE_ID, "apiKey");
        if (apiKey) {
            const footer = document.createDocumentFragment();
            const headerText = document.createTextNode(MODULE_TITLE);
            const headerTitle = document.createElement('h1');
            headerTitle.className = 'savagedus-title';
            headerTitle.appendChild(headerText);

            footer.append(headerTitle);


            const buttonGrid = document.createElement('div');
            buttonGrid.className = 'button-grid';

            buttonGrid.append(footer);
            const gearImportButton = document.createElement('button');
            gearImportButton.innerHTML = `<i class="fas fa-cloud-download-alt"></i>Gear`;
            buttonGrid.append(gearImportButton);
            // Button for API Import handling

            gearImportButton.addEventListener('click', async ev => {
                let selectedResults = [];
                let books = await fetchSavagedusData(apiKey, 'books');
                const content = await renderTemplate("modules/savagedus-importer/templates/gear-search.hbs", books);

                let savagedInPrompt = new SavagedImport({
                    title: `${MODULE_TITLE} - Import Gear`,
                    content: content,
                    buttons: {
                        "Import": {
                            icon: `<i class="fas fa-cloud-download-alt"></i>`,
                            label: "Import",
                            callback: async () => {
                                selectedResults = getSelectedOptions(document.getElementById('savagedus-gear-results-list'));
                                const selectedItemType = getSelectedOptions(document.getElementById('savagedus-type-select'))[0];
                                // Create new import folder
                                let newFolder = await createImportFolder('Item');
                                for (const selectedResult of selectedResults) {
                                    let resultData = SavagedImport.results.find(result => result.uuid === selectedResult);
                                    importItemData(resultData, { method: 'API', folder: newFolder }, selectedItemType);
                                }
                            }
                        },
                        "Cancel": {
                            icon: `<i class="fas fa-window-close"></i>`,
                            label: "Cancel",
                            callback: () => { }
                        }
                    },
                    render: async (html) => {
                        styleAppWindowDialog(html);
                    },
                    close: async () => { }
                }, { popout: true, width: 540, resizable: false });
                savagedInPrompt.render(true);
            });
            //TODO: default buttons so people can hit enter to import or search
            html.find('.directory-footer').append(buttonGrid);
        }
    }
});

Hooks.on('getActorDirectoryEntryContext', (html, contextOptions) => {
    contextOptions.push({
        name: 'Import Update from Savaged.us',
        icon: '<i class="fas fa-cloud-download-alt"></i>',
        condition: (li) => {
            const apiKey = game.settings.get(MODULE_ID, 'apiKey');
            const targetActorId = li[0].dataset.documentId;
            const actor = game.actors.get(targetActorId);
            const gmRole = game.users.get(game.userId).role;
            return gmRole > 2 && apiKey !== '' && actor.getFlag(MODULE_ID, 'app') === 'Savaged.us' && actor.getFlag(MODULE_ID, 'uuid') !== undefined;
        },
        callback: async (li) => {
            const actorId = li[0].dataset.documentId;
            const actor = game.actors.get(actorId);

            await updateActor(actor);
        },
    });
});

async function importCharacter(apiKey, actorType, executePatchActors) {
    let title = `${MODULE_TITLE} - Import Characters`;
    if (actorType === 'npc') {
        title = `${title} as NPCs`;
    }
    let sortedCharacters;
    let content = await renderTemplate("modules/savagedus-importer/templates/character-list.hbs", sortedCharacters);

    let savagedInPrompt = new Dialog({
        title: title,
        content: content,
        buttons: {
            "Import": {
                icon: `<i class="fas fa-cloud-download-alt"></i>`,
                label: 'Import',
                callback: async (e) => {
                    const selectedCharacters = getSelectedOptions(document.getElementById('savagedus-character-list'));
                    const asNPC = document.getElementById('savagedus-as-npc').checked;
                    if (asNPC) {
                        actorType = 'npc';
                    }
                    // Create Folder for new imports
                    let folder = await createImportFolder('Actor');
                    let folderName = folder.name;

                    for (const selectedCharacter of selectedCharacters) {
                        let characterData = characters.find(character => character.uuid === selectedCharacter);
                        if (typeof characterData !== 'undefined') {
                            const uuid = characterData.uuid;
                            // update
                            if (uuid !== '') {
                                const existingActor = await game.actors.find((a) => a.getFlag(MODULE_ID, 'uuid') === uuid);
                                if (existingActor) {
                                    const lastUpdated = await existingActor.getFlag(MODULE_ID, 'updated');
                                    if (lastUpdated !== characterData.updatedDate) {
                                        // Get the existing actor's current folder if it exists
                                        let parentFolder = existingActor.folder;
                                        if (parentFolder === null && (characterData.alliedExtras.length || characterData.vehicles.length)) {
                                            // If it doesn't exist, create a new import folder
                                            parentFolder = await createImportFolder('Actor');
                                        }

                                        // Identify whether updating a character or NPC
                                        let updatedActor = await importCharacterData(characterData, existingActor, { method: 'API', folder: parentFolder, itemsFolderName: folderName }, existingActor.type);
                                        await existingActor.update(updatedActor);
                                        // Add potential AEs
                                        appendActiveEffects(existingActor, characterData);
                                        // Set as driver of vehicles
                                        if (characterData.vehicles.length) {
                                            setDriverID(existingActor);
                                        }
                                        await ChatMessage.create({ content: `<p>@Actor[${existingActor.id}] has been updated.</p>` });
                                    }
                                } else {
                                    // Use new import folder
                                    let newActor = await importCharacterData(characterData, null, { method: 'API', folder: folder, itemsFolderName: folderName }, actorType);
                                    newActor.folder = folder.id;
                                    const actor = await Actor.create(newActor);
                                    // Add potential AEs
                                    appendActiveEffects(actor, characterData);

                                    // Set as driver of vehicles
                                    if (characterData.vehicles.length) {
                                        await setDriverID(actor);
                                    }
                                }
                            }
                        }
                    }

                    if (folder.contents.length === 0 && folder.children.length === 0) {
                        await folder.delete();
                    }

                    if (executePatchActors) {
                        patchActorsDialog();
                    }
                }
            },
            "Cancel": {
                icon: `<i class="fas fa-window-close"></i>`,
                label: 'Cancel',
                callback: () => { }
            }
        },
        render: async html => {
            styleAppWindowDialog(html);
        },
        close: async () => { }
    });
    savagedInPrompt.render(true);

    // Now fetch the data and rerender
    const characters = await fetchSavagedusData(apiKey, 'characters');
    if (characters) {
        const savagedUsActors = game.actors.filter((a) => a.getFlag(MODULE_ID, 'uuid'));
        sortedCharacters = sortCharacters(characters, savagedUsActors);
        content = await renderTemplate("modules/savagedus-importer/templates/character-list.hbs", sortedCharacters);
    }
    savagedInPrompt.data.content = content;
    savagedInPrompt.options.height = 'auto';
    savagedInPrompt.render(true);
}

async function updateActor(actor) {
    //A second check in case the API key is reset while the actor sheet is open.
    const loadingResults = $('<i style="display: none;" class="fas fa-spinner fa-spin"></i>')[0];
    const apiKey = game.settings.get(MODULE_ID, "apiKey");
    const patchAllActorsAvailable = typeof patchActorsDialog !== 'undefined';
    const patchActors = patchAllActorsAvailable ? game.settings.get('savagedus-importer', 'patchActors') : false;
    const executePatchActors = patchAllActorsAvailable && patchActors;
    const actorName = actor.name;
    const actorId = actor.id;
    const uuid = actor.getFlag(MODULE_ID, 'uuid');

    if (apiKey) {
        loadingResults.style.display = '';
        let characterData = await fetchSavagedusData(apiKey, 'character', { uuid: uuid });

        // Create Folder for new imports
        let folderName = await createImportedFolderName();
        if (!(characterData === null)) {
            let confirmationMessage = '',
                dialogContent = '',
                dialogTitle = '';
            if (actor.getFlag('savagedus-importer', 'updated') != characterData.updatedDate) {
                dialogTitle = `${MODULE_TITLE} - Update ${actorName}`;
                dialogContent = `<p>An update is available for ${actorName}. Would you like to import the update?</p>`;
                confirmationMessage = `<p>@Actor[${actorId}]  has been updated!</p>`;
            } else {
                dialogTitle = `${MODULE_TITLE} - No Updates for ${actorName}`;
                dialogContent = `<p>${actorName} has no update available. Would you like to refresh the character anyway?</p>`;
                confirmationMessage = `<p>@Actor[${actorId}]  has been updated!</p>`;
            }
            const savagedInPrompt = Dialog.confirm({
                title: dialogTitle,
                content: dialogContent,
                yes: async () => {
                    let existingActor = game.actors.get(actorId);
                    let subfolder = actor.folder;
                    if (subfolder === null && (characterData.alliedExtras.length || characterData.vehicles.length)) {
                        subfolder = await createImportFolder('Actor');
                    }
                    let updatedActor = await importCharacterData(characterData, existingActor, { method: 'API', folder: subfolder, itemsFolderName: folderName }, actor.type);
                    await existingActor.update(updatedActor);
                    // Add potential AEs
                    appendActiveEffects(actor, characterData);

                    // Set as driver of vehicles
                    if (characterData.vehicles.length) {
                        setDriverID(existingActor);
                    }

                    ChatMessage.create({ content: confirmationMessage });

                    if (executePatchActors) {
                        patchActorsDialog();
                    }
                },
                no: () => { },
                defaultYes: false
            });
        } else {
            const savagedInPrompt = new Dialog({
                title: `${MODULE_TITLE} - ${actorName} Not Found`,
                content: `<p>Cannot retrieve character. Either the character does not exist in the Savaged.us account or the API key is incorrect.</p>
                                <ul>
                                    <li>Character Name: <code>${actorName}</code></li>
                                    <li>UUID: <code>${uuid}</code></li>
                                </ul>`,
                buttons: {
                    "Close": {
                        icon: `<i class="fas fa-window-close"></i>`,
                        label: "Close",
                        callback: () => { }
                    }
                }
            });
            savagedInPrompt.render(true);
        }
    }
}

async function appendActiveEffects(actor, responseData) {
    const incomingSavagedEffectData = {
        "changes": [],
        "disabled": false,
        "icon": "/modules/savagedus-importer/assets/svgd-us-small.png",
        "label": `${MODULE_TITLE} Effects`,
        "transfer": false,
        "flags": {
            MODULE_ID: {
                "createdDate": responseData.createdDate,
                "updatedDate": responseData.updatedDate,
            }
        }
    };
    const priority = 999;
    // Check to see if Savaged.us effects already exist.
    const existingSavagedEffect = actor.effects.size ? await actor.effects.find(e => e.getFlag(MODULE_ID, 'createdDate') || e.label.includes('Savaged.us')) : false;
    if (existingSavagedEffect) {
        // Delete them
        await existingSavagedEffect.delete();
    }
    // Check if AE for extra bennies is needed
    const maxBenniesKey = 'system.bennies.max';
    const currentMaxBennies = actor.system.bennies.max;
    const responseDataMaxBennies = responseData.benniesMax;
    const maxBenniesDiff = responseDataMaxBennies === 0 ? 0 : responseDataMaxBennies - currentMaxBennies;
    let maxBenniesChanges;

    if (maxBenniesDiff !== 0) {
        maxBenniesChanges = {
            'key': maxBenniesKey,
            'value': maxBenniesDiff,
            'mode': 2,
            'priority': priority
        };
        incomingSavagedEffectData.changes.push(maxBenniesChanges);
    }

    // Check if AE for modified Pace is needed
    const paceKey = 'system.stats.speed.value';
    const currentPace = actor.system.stats.speed.value;
    const responseDataPace = responseData.paceTotal;
    const paceDiff = responseDataPace - currentPace;
    let paceChanges;

    if (paceDiff !== 0) {
        paceChanges = {
            'key': paceKey,
            'value': paceDiff,
            'mode': 2,
            'priority': priority
        };
        incomingSavagedEffectData.changes.push(paceChanges);
    }

    // Check if AE for modified Running Die is needed
    const runningDieKey = 'system.stats.speed.runningDie';
    const currentRunningDie = parseInt(actor.system.stats.speed.runningDie);
    const responseRunningDie = parseInt(responseData.runningDie.split('d')[1]);
    const runningDieDiff = responseRunningDie - currentRunningDie;
    let runningDieChanges;

    if (runningDieDiff !== 0) {
        runningDieChanges = {
            'key': runningDieKey,
            'value': runningDieDiff,
            'mode': 2,
            'priority': priority
        };
        incomingSavagedEffectData.changes.push(runningDieChanges);
    }

    // Check if AE for modified Armor is needed
    const armorKey = 'system.stats.toughness.armor';
    const currentArmor = parseInt(actor.system.stats.toughness.armor);
    const responseArmor = parseInt(responseData.armorValue);
    const armorDiff = responseArmor - currentArmor;
    let armorChanges;

    if (armorDiff !== 0) {
        armorChanges = {
            'key': armorKey,
            'value': armorDiff,
            'mode': 2,
            'priority': priority
        };
        incomingSavagedEffectData.changes.push(armorChanges);
    }

    if (incomingSavagedEffectData.changes.length) {
        await actor.createEmbeddedDocuments('ActiveEffect', [incomingSavagedEffectData]);
    }
}

function styleAppWindowDialog(html) {
    const dialogWindow = html[0].parentElement.parentElement;
    const currentClass = dialogWindow.className;
    if (!currentClass.includes(MODULE_ID)) {
        dialogWindow.className = `${dialogWindow.className} ${MODULE_ID}`;
        if (game.settings.get('savagedus-importer', 'enableTheme')) {
            dialogWindow.className = `${dialogWindow.className} savagedus-theme`;
        }
    }
}

Handlebars.registerHelper('updated', function (characterData) {
    const existingActor = game.actors.find((a) => a.getFlag(MODULE_ID, uuid) === characterData.uuid);
    return characterData.uuid === existingActor.getFlag(MODULE_ID, 'uuid');
});