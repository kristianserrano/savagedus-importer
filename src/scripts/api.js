const apiUrl = "https://savaged.us/_api/auth";
const headers = new Headers();
headers.append("Content-Type", "application/json; charset=utf-8");
const requestOptions = {
    method: 'POST',
    headers: headers,
    redirect: 'follow'
};

export const fetchSavagedusData = async function fetchSavagedusData(apiKey, type, params = {}) {
    if (!apiKey) {
        apiKey = game.settings.get(MODULE_ID, "apiKey");
    }

    let apiPath = '';
    const optionsBody = { apikey: apiKey };

    switch (type) {
        case 'books':
            apiPath = '/book-list/';
            break;
        case 'characters':
            apiPath = '/get-characters-generic-json/';
            break;
        case 'character':
            apiPath = '/get-character-by-uuid-generic-json/';
            optionsBody.uuid = params.uuid;
            break;
        case 'bestiary':
            apiPath = '/search-bestiary-generic-json/';
            optionsBody.books = params.books;
            optionsBody.search = params.searchString;
            optionsBody.bestiary_only = true;
            break;
        case 'gear':
            apiPath = '/search-gear-generic-json/';
            optionsBody.books = params.books;
            optionsBody.search = params.searchString;
            break;
        case 'weapons':
            apiPath = '/search-weapons-generic-json/';
            optionsBody.books = params.books;
            optionsBody.search = params.searchString;
            break;
        case 'armor':
            apiPath = '/search-armor-generic-json/';
            optionsBody.books = params.books;
            optionsBody.search = params.searchString;
            break;
        case 'shields':
            apiPath = '/search-shields-generic-json/';
            optionsBody.books = params.books;
            optionsBody.search = params.searchString;
            break;
        case 'cachebuster':
            apiPath = '/regenerate-save-caches/';
            break;
    }
    const url = apiUrl + apiPath;
    requestOptions.body = JSON.stringify(optionsBody);
    const response = await fetch(url, requestOptions).catch((e) => {
        ui.notifications.warn("Error connecting to server: " + e);
    });
    if (response) {
        const data = await response.json().catch((e) => {
            ui.notifications.warn("Error retrieving results: " + e);
        });
        return data;
    } else {
        return undefined;
    }
};

export const testConnection = async function testConnection(apiKey) {
    const url = apiUrl + '/whoami';
    // Request whoami (POST https://savaged.us/_api/auth/whoami)
    const optionsBody = { apikey: apiKey };
    requestOptions.body = JSON.stringify(optionsBody);
    const response = await fetch(url, requestOptions).catch((e) => {
        ui.notifications.warn("Network error connecting to server: " + e);
        return false;
    });
    if (response.ok) {
        const data = JSON.parse(await response.json());
        if (data.name.length > 2) {
            ui.notifications.info("Savaged.us: API key is valid!");
            return true;
        } else {
            ui.notifications.warn("Savaged.us: API key is not valid.");
            return false;
        }
    } else {
        ui.notifications.warn("Received error response from remote server: " + response.status + " - " + response.statusText);
        return false;
    }
};


//test()