import { testConnection } from "./api.js";
import { MODULE_ID } from "./../savagedus-importer.js";

export const registerSettings = async function () {
    // Register any custom module settings here

    const user = await game.data.users.find(u => u._id === game.userId);
    if (user.role > 2) {
        game.settings.register(MODULE_ID, "apiKey", {
            name: "Savaged.us API Key",
            hint: "To import characters and bestiary entries directly, please enter your Savaged.us API key.",
            type: String,
            default: '',
            scope: "client",
            config: true,
            onChange: async (value) => {
                if (!value || (value && await testConnection(value))) {
                    ui.actors.render(true);
                }
            }
        });
    }

    if (typeof patchActorsDialog !== 'undefined') {
        game.settings.register(MODULE_ID, "patchActors", {
            name: "Automatically Run Patch Actors Macro",
            hint: "If the Patch All Actors Macro is available, the import script will run it automatically after completion.",
            type: Boolean,
            default: false,
            scope: "world",
            config: true
        });
    }

    game.settings.register(MODULE_ID, "hasVision", {
        name: "Add Vision to Imported Actors' Tokens",
        hint: "Enables vision for character's tokens during import. This does not affect characters imported as NPCs.",
        type: Boolean,
        default: true,
        scope: "world",
        config: true
    });

    game.settings.register(MODULE_ID, "characterToughnessAutocalc", {
        name: "Calculate Toughness for Characters",
        hint: "Enables Automatic calculation of Toughness and Armor based on equipped Items. Works with Character Actor types only, not NPCs.",
        type: Boolean,
        default: true,
        scope: "world",
        config: true
    });

    game.settings.register(MODULE_ID, "updateCurrency", {
        name: "Update Currency During Character Updates",
        hint: "If you manage currency in Savaged.us, keep this setting enabled so that currency is updated during Character updates.",
        type: Boolean,
        default: true,
        scope: "world",
        config: true
    });

    game.settings.register(MODULE_ID, "updateOwnedItems", {
        name: "Update Actor's Owned Items",
        hint: "This will overwrite all owned items (including custom changes) with the imported versions.",
        type: Boolean,
        default: false,
        scope: "world",
        config: true
    });

    if (game.modules.get('polyglot')?.active) {
        game.settings.register(MODULE_ID, "importLanguages", {
            name: "Import Languages to Polyglot",
            hint: "Supports the Polyglot module by adding characters' languages to the custom languages in Polyglot's settings.",
            type: Boolean,
            default: true,
            scope: "world",
            config: true
        });
    }

    game.settings.register(MODULE_ID, "addImportData", {
        name: "Append Savaged.us Importer Info",
        hint: "Appends import info to an actor's details on the actor sheet. Useful for debugging.",
        type: Boolean,
        default: false,
        scope: "world",
        config: true
    });

    game.settings.register(MODULE_ID, "enableTheme", {
        name: "Apply Savaged.us Theme",
        hint: "Applies the Savaged.us theme to the Savaged.us Importer module windows.",
        type: Boolean,
        default: false,
        scope: "world",
        config: true
    });
};