export const getSelectedOptions = function getSelectedOptions(selectElement) {
    let selectedOptions = [];
    for (let i = 0; i < selectElement.options.length; i++) {
        if (selectElement.options[i].selected) {
            selectedOptions.push(selectElement.options[i].value);
        }
    }
    return selectedOptions;
};

export const sortCharacters = function sortCharacters(characters, savagedUsActors) {
    let data = {
        new: {
            label: 'New',
            list: []
        },
        updated: {
            label: 'Update Available',
            list: []
        },
        synched: {
            label: 'No Update Available',
            list: []
        }
    };
    for (const c of characters) {
        // foundry character
        const existingActor = savagedUsActors.find((a) => a.getFlag('savagedus-importer', 'uuid') === c.uuid);
        if (existingActor) {
            //TODO: compare time instead of string and only update if savaged is newer ( otherwise alert )
            if (existingActor.getFlag('savagedus-importer', 'updated') !== c.updatedDate) { // if true check if character is out of date
                data.updated.list.push(c); // if true put in update category
            } else {
                data.synched.list.push(c); // otherwise place in synced category
            }
        } else {
            data.new.list.push(c); // place in new category
        }
    }
    return data;
};

export const createImportedFolderName = async function createImportedFolder() {
    const now = new Date();
    const YYYY = now.getFullYear(),
        MM = ('0' + now.getMonth() + 1).slice(-2),
        DD = ('0' + now.getDate()).slice(-2),
        hh = ('0' + now.getHours()).slice(-2),
        mm = ('0' + now.getMinutes()).slice(-2),
        ss = ('0' + now.getSeconds()).slice(-2);

    return `Import ${YYYY}-${MM}-${DD}T${hh}:${mm}:${ss}`;
};
export const createImportFolder = async function createImportFolder(type, folderName, parent = null) {
    if (typeof folderName === 'undefined') {
        folderName = await createImportedFolderName();
    }
    const newFolder = await Folder.create({
        name: folderName,
        type: type,
        parent: parent
    });
    return newFolder;
};

export const sortResults = async function sortResults(results) {
    let sortedResults = {};
    results.forEach(result => {
        if (result.bookName in sortedResults) {
            sortedResults[result.bookName].push(result);
        } else {
            sortedResults[result.bookName] = [result];
        }
    });
    return sortedResults;
};

export const checkDirectory = async function checkDirectory(path) {
    //Should source should be set globally?
    let dest = typeof ForgeVTT === 'undefined' ? 'data' : 'forgevtt';
    await makeDirs(dest, path);
    let dir = await FilePicker.browse(dest, path);
    return dir.target;
};

async function makeDirs(dest, fullPath) {
    let pathBuild = '';
    for (const path of fullPath.split('/')) {
        pathBuild += path + '/';
        await FilePicker
            .createDirectory(dest, pathBuild, {})
            .catch((error) => {
                if (!error.includes('EEXIST')) {
                    console.error(...ERROR_PREFIX, error);
                }
            });
    }
}

export async function uploadImages(inData, imageURL) {
    let dest = typeof ForgeVTT === 'undefined' ? 'data' : 'forgevtt';
    let fileName = '';
    let imagesPath = 'savagedus-importer/imported-images';
    let uploadPath = imagesPath + '/' + inData.name.replace(/[^a-z0-9]/gi, '_').toLowerCase();
    let fullPath = 'icons/svg/mystery-man.svg';
    checkDirectory(imagesPath); //Check for and create the images directory
    await fetch(imageURL)
        .then(async function (response) {
            if (response.ok) {
                await response.blob().then(async function (image) {
                    if (image.type.startsWith('image')) {
                        let url = new URL(imageURL);
                        let params = new URLSearchParams(url.search);
                        params.delete('v');
                        fileName = url.pathname.split('/').pop();
                        //Create a File from the Blob
                        image = new File([image], fileName, { type: image.type, lastModified: new Date() });
                        // Check if directory exists.
                        let createdPath = await checkDirectory(uploadPath);
                        if (createdPath === uploadPath) {
                            let returnedPath = await FilePicker.upload(dest, uploadPath, image, {});
                            if (returnedPath.status === 'success') {
                                fullPath = returnedPath.path;
                            }
                        }
                    } else {
                        ui.notifications.error(`Savaged.us Importer: Image not found.`);
                    }
                });
            }
        }).catch(async error => {
            // Image couldn't be fetched, so replace it. No more hotlinking
            ui.notifications.warn('Savaged.us Importer: Host server blocked the fetch request for the image.');
        });
    return fullPath;
}

function loadImage(url, timeout = 10000) {
    return new Promise(function (resolve) {
        try {
            const image = new Image();
            let expireTime = Date.now();
            image.addEventListener("load", function () {
                resolve(true);
            });
            image.addEventListener("error",
                function (error) {
                    const endTime = Date.now();
                    if (endTime > expireTime) {
                        resolve("timeout");
                    }
                    resolve("load error " + url);
                });
            image.src = url;
            expireTime = Date.now() + timeout;
        } catch (err) {
            resolve("load exception " + url);
        }
    });
}
