
import { uploadImages } from './utils.js';
import { MODULE_ID, MODULE_TITLE } from "./../savagedus-importer.js";

export const importItemData = async function (inData, options, inType) {
    const tempInData = inData;
    inData = {
        gear: [],
        weapons: [],
        armor: [],
        shields: []
    };
    switch (inType) {
        case 'gear':
            inData.gear.push(tempInData);
            break;
        case 'weapons':
            inData.weapons.push(tempInData);
            break;
        case 'armor':
            inData.armor.push(tempInData);
            break;
        case 'shields':
            inData.shields.push(tempInData);
            break;
    }
    mapItems(inData, null, options.folder.name, inType);
};

export const importCharacterData = async function (inData, existingActor = null, options, inType = "character") {
    if (typeof options.folder === 'undefined') {
        options.folder = null;
    }

    if (typeof inData.wildcard === 'undefined') {
        inData.wildcard = false;
    }

    if (typeof inData.playerCharacter === 'undefined') {
        if (inData.rankName === 'Bestiary') {
            inData.playerCharacter = false;
        } else {
            inData.playerCharacter = true;
        }
    }

    let syncable = false;

    if (inData.playerCharacter === true && inType === 'npc') {
        syncable = true;
    }

    // Import Allied Extras
    if (inData.alliedExtras && inData.alliedExtras.length > 0) {
        let owningActor = game.actors.find(actor => actor.system.type === 'character' && actor.getFlag(MODULE_ID, 'uuid') === inData.uuid)
        let alliesSubfolder = null;
        for (const allyData of inData.alliedExtras) {
            // Look to see if ally Actor already exists
            let existingAlly = game.actors.find(actor => actor.system.type === 'npc' && actor.getFlag(MODULE_ID, 'uuid') === allyData.uuid);
            if (existingAlly === undefined) {
                if (alliesSubfolder === null) {
                    let folderName = `${inData.name}'s Allies`;
                    alliesSubfolder = await Folder.create({
                        name: folderName,
                        type: 'Actor',
                        parent: options.folder
                    });
                }
                let newActor = await importCharacterData(allyData, null, { "ownerUUID": inData.uuid, itemsFolderName: options.itemsFolderName }, "npc");
                newActor.folder = alliesSubfolder.id;
                //Sets ownership to match ally's owning actor's ownership
                if (owningActor) {
                    // Sets Actor ownership to match ally's owning Actor ownership
                    newActor.ownership = owningActor.ownership;
                }
                await Actor.create(newActor);
            } else {
                let updatedAlly = await importCharacterData(allyData, existingAlly, { "ownerUUID": inData.uuid, itemsFolderName: options.itemsFolderName }, "npc");
                //updatedAlly.id = existingAlly.id //Error kept coming up without the actor.id being added.
                await existingAlly.update(updatedAlly);
                if (owningActor) {
                    // Sets Actor ownership to match ally's owning Actor ownership
                    await existingAlly.update({ ownership: owningActor.ownership });
                }
            }
        }
        //Sets ownership to match ally's owning Actor's ownership
        if (owningActor && alliesSubfolder !== null) {
            // Sets Folder ownership to match ally's owning Actor ownership
            alliesSubfolder.ownership = owningActor.ownership;
            await Folder.update(alliesSubfolder);
        }
    }

    // Set Image
    let updatableActor = game.actors.find(actor => actor.system.type === inType && actor.getFlag(MODULE_ID, 'uuid') === inData.uuid);
    if (inData.image !== '') {
        inData.image = await uploadImages(inData, inData.image);
    } else if (updatableActor && updatableActor.img !== '' && inData.image === '') {
        inData.image = updatableActor.img;
    } else {
        inData.image = 'icons/svg/mystery-man.svg';
    }

    // Get OwnerID (present if processing an Allied Extra)
    let ownerUUID = '';

    if (typeof options.ownerUUID !== 'undefined') {
        ownerUUID = options.ownerUUID;
    }

    // Map token data
    let tokenData = await mapToken(inData, options, inType);

    // Set automatic calculation of Toughness based on character type and user preferences
    let autoCalcToughness = false;

    if (inData.playerCharacter === true && game.settings.get(MODULE_ID, "characterToughnessAutocalc")) {
        autoCalcToughness = true;
    }

    // Get new character Items in advance for use with other data values
    let items = await mapItems(inData, existingActor, options.itemsFolderName, inType);

    // Build Actor data
    let outData = {
        name: inData.name,
        type: inType,
        img: inData.image,
        token: tokenData,
        sort: 12000,
        system: {
            attributes: mapAttributes(inData.attributes),
            stats: mapStats(inData),
            details: mapDetails(inData, existingActor, inType),
            powerPoints: mapPowerPoints(inData),
            fatigue: mapFatigue(inData),
            wounds: mapWounds(inData),
            initiative: mapInitiative(inData, existingActor),
            advances: mapAdvances(inData),
            bennies: mapBennies(inData, inType),
            wildcard: inData.wildcard
        },
        items: items,
        flags: {
            [MODULE_ID]: {
                app: 'Savaged.us',
                method: options.method,
                appVersion: inData.appVersion,
                importer: 'Savaged.us Importer module',
                created: inData.createdDate,
                updated: inData.updatedDate,
                uuid: inData.uuid,
                ownerUUID: ownerUUID,
                isSyncable: syncable
            }
        }
    };

    outData.system.details.autoCalcToughness = autoCalcToughness;

    // Add owned vehicles
    if (inData.vehicles && inData.vehicles.length > 0) {
        let owningActor = game.actors.find(actor => actor.system.type === 'character' && actor.getFlag(MODULE_ID, 'uuid') === inData.uuid)
        let vehiclesSubfolder = null;
        for (const vehicleData of inData.vehicles) {
            let existingVehicle = game.actors.find(actor => actor.system.type === 'vehicle' && actor.system.name === vehicleData.name);
            if (existingVehicle === undefined) {
                if (vehiclesSubfolder === null) {
                    let folderName = `${inData.name}'s Vehicles`;
                    vehiclesSubfolder = await Folder.create({
                        name: folderName,
                        type: 'Actor',
                        parent: options.folder
                    });
                }
                let newVehicle = await importVehicleData(vehicleData, existingVehicle, options.itemsFolderName, { "owningActor": inData });
                newVehicle.folder = vehiclesSubfolder.id;
                //Sets ownership to match vehicle's owning Actor's ownership
                if (owningActor) {
                    // Sets Actor ownership to match vehicles's owning Actor ownership
                    newVehicle.ownership = owningActor.ownership;
                }
                await Actor.create(newVehicle);
            } else {
                let updatedVehicle = await importVehicleData(vehicleData, existingVehicle, options.itemsFolderName, { "owningActor": inData });
                updatedVehicle.id = existingVehicle.id; //Error kept coming up without the actor.id being added.
                if (owningActor) {
                    // Sets Actor ownership to match ally's owning Actor ownership
                    updatedVehicle.ownership = owningActor.ownership;
                }

                await existingVehicle.update(updatedVehicle);
            }
        }
        //Sets ownership to match vehicle's owning Actor's ownership
        if (owningActor && vehiclesSubfolder !== null) {
            // Sets Folder ownership to match vehicles's owning Actor ownership
            vehiclesSubfolder.ownership = owningActor.ownership;
            await Folder.update(vehiclesSubfolder);
        }
    }

    // If "Polyglot" module is installed and active and import languages enabled, import languages
    const polyglotActive = game.modules.get('polyglot')?.active;
    const importToPolyglot = polyglotActive ? game.settings.get(MODULE_ID, 'importLanguages') : false;
    const executeImportLanguages = polyglotActive && importToPolyglot;

    if (executeImportLanguages) {
        await game.settings.set('polyglot', 'customLanguages', await importLanguages(inData));
    }

    return outData;
};

function writeDescription(item) {
    let description = '';
    let source = '';
    if (item.descriptionHTML && item.descriptionHTML !== '') {
        description = item.descriptionHTML;
    } else {
        // Get source property name
        if (item.takenFrom) { //If the new takenFrom property exists, use it.
            source = "takenFrom";
        } else {
            source = "book_name_page"; //Default to book_name_page otherwise
        }
        // Create source property and set value to formatted text.
        if (item[source]) {
            item.source = `<p><cite>${item[source]}</cite></p>`;
        }

        if (item.summary && (!item.description || item.description === '')) {
            description = `<p>${item.summary}</p>`;
        } else if ((!item.summary || item.summary === '') && item.description) {
            if (item.source) {
                description = `${item.description}${item.source}`;
            } else {
                description = item.description;
            }
        } else {
            if (item.source) {
                description = item.source;
            }
        }
    }
    return description;
}

async function mapToken(inData, options, inType) {
    let tokenData = {};
    if (game.settings.get(MODULE_ID, "hasVision") && (inType === 'character' || typeof options.ownerUUID !== 'undefined')) {
        tokenData.vision = true;
    }
    if (inType === 'character' || typeof options.ownerUUID !== 'undefined') {
        tokenData.disposition = 1;
        tokenData.actorLink = true;
    }
    if (inData.imageToken === '') {
        let updatableActor = game.actors.find(actor => actor.system.type === inType && actor.getFlag(MODULE_ID, 'uuid') === inData.uuid);
        if (updatableActor) {
            tokenData.img = updatableActor.system.token.img;
        } else {
            tokenData.img = 'icons/svg/mystery-man.svg';
        }
    } else {
        tokenData.img = await uploadImages(inData, inData.imageToken);
    }
    tokenData.name = inData.name;
    return tokenData;
}

function mapAttributes(attr) {
    let data = {};
    attr.forEach(attribute => {
        data[attribute.name] = {};
        data[attribute.name].die = {};
        data[attribute.name].die.sides = attribute.dieValue;
        if (attribute.name === "smarts" && attribute.value.includes('(A)')) {
            data[attribute.name].animal = true;
        }
        data[attribute.name].die.modifier = attribute.mod;
        data[attribute.name]["wild-die"] = {};
        data[attribute.name]["wild-die"].sides = 6;
    });
    return data;
}

async function mapItems(inData, existingActor, folderName, inType, containerItem = null) {
    // Get arrays of items first before concatenating
    let items = [];
    const gearItemImport = inType === 'gear' || inType === 'weapons' || inType === 'armor' || inType === 'shields';
    if (gearItemImport) {
        switch (inType) {
            case 'gear':
                items = items.concat(items, await mapGear(inData, existingActor, folderName, inType, containerItem));
                break;
            case 'weapons':
                items = items.concat(items, await mapWeapons(inData, inType));
                break;
            case 'armor':
                items = items.concat(items, await mapArmor(inData, inType));
                break;
            case 'shields':
                items = items.concat(items, await mapShields(inData, inType));
                break;
        }
    } else {
        let itemGroups = [];

        if (containerItem !== null || inType === 'vehicle') {
            itemGroups = [
                await mapGear(inData, existingActor, folderName, inType, containerItem),
                await mapWeapons(inData),
                await mapArmor(inData, inType),
                await mapShields(inData)
            ];
        } else {
            itemGroups = [
                await mapHindrances(inData, existingActor),
                await mapSkills(inData),
                await mapEdges(inData, existingActor),
                await mapSpecialAbilities(inData),
                await mapGear(inData, existingActor, folderName, inType, containerItem),
                await mapWeapons(inData),
                await mapArmor(inData, inType),
                await mapShields(inData),
                await mapCyberware(inData),
                await mapPowers(inData)
            ];
        }
        // Now concatenate the arrays into one array
        for (const group of itemGroups) {
            if (typeof group !== 'undefined' && group.length) {
                items.push.apply(items, group);
            }
        }
    }

    // Create Items from character's items.
    const importDirectoryItems = gearItemImport;
    const updateOwnedItems = game.settings.get(MODULE_ID, "updateOwnedItems") && inType === 'character';
    let itemsFolder = game.folders.find(folder => folder.type === 'Item' && folder.name === folderName);

    if (items.length > 0) {
        for (let item of items) {
            // Check if Item is missing a name and put in a placeholder to avoid errors.
            if (item.name === '') {
                item.name = 'Item Name Missing'
            }

            if (item.contains?.gear) {
                items.concat(await mapItems(item.contains, existingActor));
            }

            if (importDirectoryItems) {
                // Check if folder exists
                if (itemsFolder === undefined) {
                    // Create a new timestamped folder
                    itemsFolder = await Folder.create({
                        name: folderName,
                        type: 'Item',
                        parent: null
                    });
                }
            }

            // Check if existing actor
            if (existingActor !== null) {

                // Check actor's Items for matching name and type
                let existingOwnedItem = null;
                for (const ownedItem of existingActor.items) {
                    if (ownedItem.name === item.name && ownedItem.type === item.type) {
                        existingOwnedItem = ownedItem;
                    } else {
                        // For existing skills that were once renamed with : to replace parentheses
                        if (ownedItem.type === 'skill' && ownedItem.name === item.name.replace(' (', ': ').replace(')', '')) {
                            existingOwnedItem = ownedItem;
                        }
                    }
                }

                // If there is no such owned Item...
                if (existingOwnedItem === null) {
                    // Create the owned Item
                    await existingActor.createEmbeddedDocuments('Item', [item], { renderSheet: false });
                } else {
                    // If the Item is a skill and die sides or modifier differ ...
                    const isSkill = existingOwnedItem.type === 'skill' && item.type === 'skill';
                    if (isSkill) {
                        const isSameDieType = existingOwnedItem.system.die.sides === item.system.die.sides;
                        const currentModifierNull = existingOwnedItem.system.die.modifier === null;
                        const isSameSkillModifier = existingOwnedItem.system.die.modifier === item.system.die.modifier;

                        // Check if the existing owned item and incoming item data are the same.
                        if (currentModifierNull || !isSameDieType || !isSameSkillModifier) {
                            // Update the die sides value; includes name in case the skill name needs to be returned to parentheses instead of colons.
                            await existingOwnedItem.update({
                            'system.die.sides': item.system.die.sides,
                                'system.die.modifier': item.system.die.modifier,
                                'name': item.name
                            });
                        }
                    }
                    // If update owned Items setting is true and this is a character and not an ally
                    if (updateOwnedItems) {
                        // Clear additional actions before updating with new data.
                        if (typeof existingOwnedItem.system.actions !== 'undefined') {
                            //item.id = existingOwnedItem.id
                            await existingOwnedItem.update(item);
                        }

                        // Capture the current equipped state.
                        item.system.equipStatus = existingOwnedItem.system.equipStatus;
                        item.system.favorite = existingOwnedItem.system.favorite;

                        // Update owned Item
                        //item.id = existingOwnedItem.id
                        await existingOwnedItem.update(item);
                    }
                }
            } else {
                if (importDirectoryItems) {
                    // Import items into new folder
                    await importDirectoryItem(item, itemsFolder, gearItemImport);
                }
            }
        }

        if (importDirectoryItems && itemsFolder.children.length === 0 && itemsFolder.contents.length === 0) {
            await itemsFolder.delete();
        }
    }

    // Send all items to new character data
    if (existingActor === null) {
        return items;
    }
}

function mapInitiative(inData, existingActor) {
    /*
       Build initiative tweaks only if it's a new character.
       This is to avoid overwriting updated character data applied with the mapEdges() and mapHindrances() functions.
    */
    if (existingActor === null) {
        let initTweaks = {
            hasQuick: false,
            hasLevelHeaded: false,
            hasImpLevelHeaded: false,
            hasHesitant: false
        };

        for (const edge of inData.edges) {
            if (edge.name === 'Quick') {
                initTweaks.hasQuick = true;
            }
            if (edge.name === 'Level Headed') {
                initTweaks.hasLevelHeaded = true;
            }
            if (edge.name === 'Level Headed, Improved') {
                initTweaks.hasImpLevelHeaded = true;
            }
        }

        for (const hindrance of inData.hindrances) {
            if (hindrance.name === 'Hesitant') {
                initTweaks.hasHesitant = true;
            }
        }

        return initTweaks;
    }
}

async function mapHindrances(inData, existingActor) {
    if (inData.hindrances) {
        const hindrances = [];
        for (const hindrance of inData.hindrances) {
            hindrance.name = hindrance.name.split(' (')[0];

            const description = writeDescription(hindrance);

            // If this is an existing actor, update its initiative tweaks.
            if (existingActor !== null) {
                switch (hindrance.name) {
                    case 'Hesitant':
                        await existingActor.update({ 'system.initiative.hasHesitant': true });
                        break;
                }
            }

            hindrances.push({
                name: hindrance.name,
                type: "hindrance",
                img: "systems/swade/assets/icons/hindrance.svg",
                system: {
                    description: description,
                    notes: hindrance.note,
                    major: hindrance.major
                }
            });
        }
        return hindrances;
    }
}

async function mapSkills(inData) {
    if (inData.skills) {
        const skills = [];
        for (const skill of inData.skills) {
            skill.icon = 'systems/swade/assets/icons/skill.svg';
            skill.description = '';
            let isCoreSkill = false;
            if (typeof skill.isCore !== 'undefined' && skill.isCore === true) {
                isCoreSkill = true;
            }

            skills.push({
                name: skill.name,
                type: "skill",
                img: skill.icon,
                system: {
                    description: skill.description,
                    attribute: skill.attribute,
                    die: {
                        sides: skill.dieValue,
                        modifier: skill.mod
                    },
                    isCoreSkill: isCoreSkill,
                    "wild-die": {
                        sides: 6
                    }
                }
            });

        }
        return skills;
    }
}

async function mapEdges(inData, existingActor) {
    if (inData.edges) {
        const edges = [];

        for (const edge of inData.edges) {
            edge.name = edge.name.split(' (')[0];
            const description = writeDescription(edge);

            // If this is an existing actor, update its initiative tweaks.
            if (existingActor !== null) {
                switch (edge.name) {
                    case 'Quick':
                        await existingActor. update({ 'system.initiative.hasQuick': true });
                        break;
                    case 'Level Headed':
                        await existingActor. update({ 'system.initiative.hasLevelHeaded': true });
                        break;
                    case 'Level Headed, Improved':
                        await existingActor. update({ 'system.initiative.hasImpLevelHeaded': true });
                        break;
                }
            }

            edges.push({
                name: edge.name,
                type: "edge",
                img: "systems/swade/assets/icons/edge.svg",
                system: {
                    description: description,
                    notes: edge.note,
                    isArcaneBackground: edge.name.includes("Arcane Background")
                }
            });
        }
        return edges;
    }
}

async function mapSpecialAbilities(inData) {
    if (inData.abilities) {
        const abilities = [];
        for (const ability of inData.abilities) {

            const description = writeDescription(ability);
            const subtype = 'special';
            const powersAbilities = ['Innate Powers', 'Powers'];
            const grantsPowers = powersAbilities.some(a => ability.name.includes(a));

            abilities.push({
                name: ability.name,
                type: 'ability',
                subtype: subtype,
                img: "systems/swade/assets/icons/ability.svg",
                system: {
                    description: description,
                    grantsPowers: grantsPowers,
                    notes: ability.note
                }
            });
        }
        return abilities;
    }
}

function mapStats(inData) {
    let toughness = {
        value: inData.toughnessTotal,
        armor: inData.armorValue,
        modifier: inData.toughnessMod - inData.size
    };

    if (inData.playerCharacter === true && game.settings.get(MODULE_ID, "characterToughnessAutocalc")) {
        toughness.value = inData.toughnessBase;
    }

    let stats = {
        speed: {
            runningDie: 6,
            value: 6
        },
        toughness: toughness,
        parry: {
            value: inData.parryBase,
            modifier: inData.parryMod
        },
        size: inData.size
    };
    return stats;
}

async function mapGear(inData, existingActor, folderName, inType) {
    let gear = [];
    for (const item of inData.gear) {
        if (item.contains && item.contains.gear) {
            gear.push.apply(gear, await mapItems(item.contains, existingActor, folderName, inType, item));
        }

        const equipStatus = await getEquippedAs(item, 'gear');

        if (item.name !== '') {
            gear.push({
                name: item.name,
                type: "gear",
                img: "systems/swade/assets/icons/gear.svg",
                system: {
                    description: writeDescription(item),
                    weight: item.weight,
                    notes: item.notes,
                    quantity: item.quantity,
                    price: item.cost,
                    equippable: true,
                    equipStatus: equipStatus
                }
            });
        }
    }
    return gear;
}

async function mapWeapons(inData, inType) {
    let weapons = [];
    for (const item of inData.weapons) {
        // Remove calibers with '.' from weapon names to more accurately match item names in Compendia
        let parenStart = item.name.indexOf(' (');
        let parenEnd = item.name.indexOf(')');
        let parenContents = item.name.substring(parenStart, parenEnd);

        if (parenContents.includes('.')) {
            item.name = item.name.substr(0, parenStart);
        } else {
            parenStart = item.name.indexOf(' (', parenStart + 1);
            parenEnd = item.name.indexOf(')', parenEnd + 1);
            parenContents = item.name.substring(parenStart, parenEnd);
            if (parenContents.includes('.')) {
                item.name = item.name.substr(0, parenStart);
            }
        }

        let name = item.name;
        let description = writeDescription(item);
        let dmgRoll = item.damage.replace(/Str/gi, '@str');
        let range = item.range;
        let shots = item.shots;
        let isEquippable = true;
        let equipStatus = await getEquippedAs(item, 'weapon');
        let favorite = equipStatus > 1;
        let skill = '';
        let actions = {};
        let additionalActions = {};
        let specialAbility = null;
        let edge = null;


        // If the weapon is a natural weapon from a special ability or edge, use that description instead
        // This conditional ignores abilities incase the weapon is from a container
        if (inData.abilities) {
            specialAbility = inData.abilities.find(ability => item.name === ability.name);
            if (typeof specialAbility !== 'undefined') {
                description = writeDescription(specialAbility);
            }
        }

        // This conditional ignores edges incase the weapon is from a container
        if (inData.edges) {
            edge = inData.edges.find(edge => item.name === edge.name);
            if (typeof edge !== 'undefined') {
                description = writeDescription(edge);
            }
        }

        //Skip weapons that are traditional Arcane Powers
        if (!item.notes.startsWith('Power, Power Points: ')) {
            if (item.notes.includes('Innate Attack')) {
                isEquippable = true;
                favorite = true;
                equipStatus = CONFIG.SWADE.CONST.EQUIP_STATE.CARRIED;
            }

            if (range === 'Melee') {
                range = '';
                skill = 'Fighting';
                shots = '';
                actions.skill = skill;
            } else if (item.thrown) {
                skill = 'Athletics';
                actions.skill = skill;
            } else {
                skill = 'Shooting';
                actions.skill = skill;
            }

            if (item.notes.startsWith('Power')) {
                inData.abs.forEach(ab => {
                    ab.powers.forEach(power => {
                        if (power.name === item.name) {
                            skill = ab.arcaneSkill;
                        }
                    });
                });
            }

            // If a weapon has profiles add additional actions
            if (item.profiles && item.profiles.length) {

                item.profiles.forEach(function (profile) {
                    // Create random key for additional action
                    let key = foundry.utils.randomID(10);

                    // Skip first profile or any profile without a name
                    if (profile.name != '') {
                        additionalActions[key] = {};
                        // Check if profile is for ranged attack/damage
                        if (profile.range && profile.range.toLowerCase() !== 'melee') {
                            additionalActions[key].type = 'skill';
                            additionalActions[key].rof = profile.rof;
                            // Check if shooting or throwing
                            if (profile.thrown_weapon) {
                                additionalActions[key].name = 'Athletics (throwing)';
                                additionalActions[key].skillOverride = 'Athletics';
                            } else {
                                additionalActions[key].name = 'Shooting';
                                additionalActions[key].skillOverride = 'Shooting';
                            }
                        }
                        // Check if damage profile
                        if (profile.damage !== item.damage && profile.damage !== '') {
                            additionalActions[key].name = profile.name;
                            additionalActions[key].type = 'damage';
                            additionalActions[key].dmgOverride = profile.damage;
                        }
                    }
                });
            }

            // Add an additional Super Power Ranged Attack with the Athletics skill for the possibility of a thrown trapping. This will go away once multiple skill actions are supported.
            if (item.notes.includes('Super Power') && item.range !== 'Melee' && !item.thrown) {
                let key = foundry.utils.randomID(10);
                additionalActions[key] = {};
                additionalActions[key].name = 'Athletics (throwing)';
                additionalActions[key].type = 'skill';
                additionalActions[key].skillOverride = 'Athletics';
            }

            weapons.push({
                name: name,
                type: "weapon",
                img: "systems/swade/assets/icons/weapon.svg",
                system: {
                    description: description,
                    weight: item.weight,
                    notes: item.notes,
                    quantity: item.quantity,
                    price: item.cost,
                    equippable: isEquippable,
                    equipStatus: equipStatus,
                    favorite: favorite,
                    range: range,
                    damage: dmgRoll,
                    rof: item.rof,
                    ap: item.ap,
                    shots: shots,
                    minStr: item.minStr,
                    armor: item.armor,
                    attribute: item.attribute,
                    actions: {
                        skill: skill,
                        skillMod: '',
                        dmgMod: '',
                        additional: additionalActions
                    }
                }
            });
        }
    }
    return weapons;
}

async function mapShields(inData, inType) {
    let shields = [];
    if (typeof inData.shields != 'undefined' && inData.shields.length) {
        for (const item of inData.shields) {
            const key = foundry.utils.randomID(10);
            const additionalActions = {
                [key]: {
                    name: 'Bash',
                    type: 'damage',
                    dmgOverride: '@str+d4'
                }
            };

            const equipStatus = await getEquippedAs(item, 'shield');

            if (item.name !== '') {
                shields.push({
                    name: item.name,
                    type: "shield",
                    img: "systems/swade/assets/icons/shield.svg",
                    system: {
                        description: writeDescription(item),
                        weight: item.weight,
                        notes: item.notes,
                        quantity: item.quantity,
                        cover: item.cover,
                        parry: item.parry,
                        minStr: item.minStr,
                        price: item.cost,
                        equippable: true,
                        equipStatus: equipStatus,
                        actions: {
                            skill: 'Fighting',
                            skillMod: '',
                            dmgMod: '',
                            additional: additionalActions
                        }
                    }
                });
            }
        }
    }
    return shields;
}

async function mapArmor(inData, inType) {
    let armor = [];

    // Loop through all regular armor
    for (const item of inData.armor) {

        let armorName = item.name;
        const equipStatus = await getEquippedAs(item, 'armor');
        // Since Armor in Foundry can be unequipped, there's no real need for (Unarmored) to be added.
        if (armorName !== '(Unarmored)') {
            armor.push({
                name: armorName,
                type: "armor",
                img: "systems/swade/assets/icons/armor.svg",
                system: {
                    description: writeDescription(item),
                    weight: item.weight,
                    quantity: item.quantity,
                    price: item.cost,
                    equippable: true,
                    equipStatus: equipStatus,
                    minStr: item.minStr,
                    armor: item.armor,
                    attribute: item.attribute,
                    isNaturalArmor: false,
                    locations: {
                        head: item.coversHead,
                        torso: item.coversTorso,
                        arms: item.coversArms,
                        legs: item.coversLegs
                    }
                }
            });
        }
    }

    // Loop through natural armor types
    if (inType === 'character' && typeof inData.naturalArmor !== 'undefined' && inData.naturalArmor.length > 0) {
        for (const item of inData.naturalArmor) {
            let description = '',
                notes = '',
                specialAbility = inData.abilities.find(ability => item.name === ability.name),
                edge = inData.edges.find(edge => item.name === edge.name);

            if (typeof specialAbility !== 'undefined') {
                description = writeDescription(specialAbility);
            }
            if (typeof edge !== 'undefined') {
                description = writeDescription(edge);
            }
            if (item.heavy === true) {
                notes = 'Heavy Armor';
            }
            armor.push({
                name: item.name,
                type: "armor",
                img: "systems/swade/assets/icons/armor.svg",
                system: {
                    description: description,
                    notes: notes,
                    weight: item.weight,
                    quantity: 1,
                    equippable: true,
                    equipStatus: true,
                    armor: item.armor,
                    isNaturalArmor: true,
                    locations: {
                        head: item.coversHead,
                        torso: item.coversTorso,
                        arms: item.coversArms,
                        legs: item.coversLegs
                    }
                }
            });
        }
    }
    return armor;
}

async function mapPowers(inData) {
    if (inData.abs) {
        let powers = [];
        for (const ab of inData.abs) {
            let equipStatus = false;
            for (const power of ab.powers) {
                let name = power.name;

                // If a weapon matches the power name and is a power, equip it by default.
                inData.weapons.forEach(weapon => {
                    if (weapon.name === power.name && weapon.notes.startsWith('Power, Power Points: ')) {
                        equipStatus = CONFIG.SWADE.CONST.EQUIP_STATE.CARRIED;
                    }
                });

                // Set Trappings
                let trappings = '';
                // If trappings are included in the data, use them
                if (typeof power.trappings !== 'undefined' && power.trappings !== '') {
                    trappings = power.trappings;
                }

                // If the bookPowerName property is available set the name to that.
                if (typeof power.bookPowerName !== 'undefined' && power.bookPowerName != '') {
                    name = power.bookPowerName;
                    // If a custom name, prepend custom name as trapping
                    if (power.name.includes('(' + power.bookPowerName + ')')) {
                        if (typeof power.trappings !== 'undefined' && power.trappings !== '') {
                            trappings = power.name.split(' (' + power.bookPowerName + ')')[0] + ': ' + trappings;
                        } else {
                            trappings = power.name.split(' (' + power.bookPowerName + ')')[0];
                        }
                    }
                }

                let rank = '';
                if (power.rank) {
                    rank = power.rank.charAt(0).toUpperCase() + power.rank.slice(1);
                }

                // Assign the arcane skill
                let skill = '';
                if (power.arcaneSkillName) {
                    // Use the power's new arcaneSkillName property
                    skill = power.arcaneSkillName;
                } else {
                    // Fallback to the AB arcane skill property
                    skill = ab.arcaneSkill;
                }

                // Use the power's new skill modifier value
                let skillMod = '';
                if (power.skillModifier) {
                    skillMod = power.skillModifier;
                } else {
                    inData.skills.forEach(skill => {
                        if (skill.name === ab.arcaneSkill) {
                            skillMod = skill.mod;
                        }
                    });
                }
                if (skillMod === 0) {
                    skillMod = '';
                }

                powers.push({
                    name: name,
                    type: "power",
                    img: "systems/swade/assets/icons/power.svg",
                    system: {
                        description: writeDescription(power),
                        pp: power.powerPoints,
                        damage: power.damage,
                        notes: '',
                        range: power.range,
                        duration: power.duration,
                        arcane: ab.name,
                        rank: rank,
                        trapping: trappings,
                        equipStatus: equipStatus,
                        favorite: false,
                        actions: {
                            skill: skill,
                            skillMod: skillMod
                        }
                    }
                });
            }
        }
        return powers;
    }
}

function mapPowerPoints(inData) {
    let pp = { value: 0, max: 0 };
    if (inData.abs.length === 1) {
        pp = {
            max: inData.abs[0].powerPointsMax
        };
    }
    if (inData.abs.length > 0) {
        inData.abs.forEach(ab => {
            pp[ab.name] = {
                max: ab.powerPointsMax
            };
        });
    }
    return pp;
}

// TODO: decide or at least give the option to treat cyberware differently - ie  Gear | Edge | power - this chooses gear
async function mapCyberware(inData) {
    let cyber = [];
    if (typeof inData.cyberware !== 'undefined') {
        for (const item of inData.cyberware) {
            cyber.push({
                name: item.name,
                type: "gear",
                img: "modules/savagedus-importer/assets/icons/cyberware.svg",
                system: {
                    description: writeDescription(item),
                    weight: 0,
                    strain: item.strain,
                    notes: item.notes,
                    quantity: item.quantity,
                    price: item.cost,
                    equippable: true,
                    equipStatus: CONFIG.SWADE.CONST.EQUIP_STATE.EQUIPPED
                }
            });
        }
    }
    return cyber;
}

function mapAdvances(inData) {
    let advancesDetails = `<ol class="savagedus-advances">`;
    const advancesList = [];

    for (const advance of inData.advances) {
        let advanceNamePrefixTrim = advance.name.substring(advance.description.indexOf(': ') + 1);
        let advanceType = 0;
        let advanceNotes = advance.description.substring(advance.description.indexOf(': ') + 1);
        const wasNotMajorHindrance = advance.name.includes('Remove Minor Hind:') && !advance.name.toLowerCase().includes('(major)');
        const wasMajorHindrance = advance.name.includes('Remove Minor Hind:') && advance.name.toLowerCase().includes('(major)');
        const removedReducedMajorHindrance = inData.advances.find((a) => a.name === advance.name && a.number < advance.number);


        if (advance.name.includes('Raise Skill:')) {
            advanceType = 1;
        } else if (advance.name.includes('Raise Skills:')) {
            advanceType = 2;
        } else if (advance.name.includes('Raise Attribute:')) {
            advanceType = 3;
        } else if (wasNotMajorHindrance) {
            advanceType = 4;
            advanceNotes = `${advanceNamePrefixTrim} removed`;
        } else if (wasMajorHindrance && removedReducedMajorHindrance) {
            advanceType = 4;
            advanceNamePrefixTrim = advance.name.substring(advance.name.indexOf(': ') + 1, advance.name.indexOf('(major)'));
            advanceNotes = `${advanceNamePrefixTrim} (minor) removed`;
        } else if (wasMajorHindrance) {
            advanceType = 4;
            advanceNotes = `${advanceNamePrefixTrim} reduced to a Minor Hindrance`;
        } else if (advance.name.includes('Remove Major Hind:')) {
            advanceType = 4;
            advanceNotes = `${advanceNamePrefixTrim} removed`;
        } else if (advance.name === ('( Unselected )')) {
            advanceType = 4;
            advanceNotes = 'Saved for removing a Major Hindrance';
        }

        advancesList.push({
            "id": foundry.utils.randomID(8),
            "type": advanceType,
            "sort": advance.number,
            "planned": false,
            "notes": advanceNotes
        });

        let advanceLabel = '';
        switch (advance.number) {
            case 1:
                advanceLabel = 'N';
                break;
            case 4:
                advanceLabel = 'S';
                break;
            case 8:
                advanceLabel = 'V';
                break;
            case 12:
                advanceLabel = 'H';
                break;
            case 16:
                advanceLabel = 'L';
                break;
            default:
                advanceLabel = advance.number;
                break;
        }
        advancesDetails += `<li><span class="bold">${advanceLabel}</span> <span>${advance.description}</span></li>`;
    }

    advancesDetails += `</ol>`;
    const adv = {
        value: inData.advancesCount,
        rank: inData.rankName,
        details: advancesDetails,
        list: advancesList
    };

    return adv;
}

function mapFatigue(inData) {
    let fat = {
        min: 0,
        max: inData.fatigueMax
    };
    return fat;
}

function mapWounds(inData) {
    let wounds = {
        min: 0,
        max: inData.woundsMax,
        ignored: 0
    };

    for (const edge of inData.edges) {
        if (edge.name === 'Nerves of Steel') {
            wounds.ignored = 1;
        }
        if (edge.name === 'Improved Nerves of Steel') {
            wounds.ignored = 2;
        }
    }
    return wounds;
}

function mapDetails(inData, existingActor, inType) {
    let descriptionHTML = '',
        backgroundHTML = '';

    let importMetadata = {
        url: inData.savagedUsShareURL,
        source: inData.bookName,
        publisher: inData.bookPublisher,
        citation: '',
        uuid: inData.uuid,
        appVersion: inData.appVersion,
        createdDate: inData.createdDate,
        updatedDate: inData.updatedDate,
        importedDate: function () {
            let now = new Date();
            return now.toUTCString();
        }
    };

    if (inData.description !== '') {
        if (inType === 'character') {
            descriptionHTML = `<h3 class="bold">Description:</h3><p class="savagedus-bio">${inData.description}</p>`;
        } else {
            descriptionHTML = `<p class="savagedus-bio">${inData.description}</p>`;
        }
    }

    if (inData.background !== '') {
        backgroundHTML = `<h3 class="bold">Background:</h3><p class="savagedus-bio">${inData.background}</p>`;
    }

    let autoCalcToughness = inData.playerCharacter;

    let wealth = {
        modifier: inData.wealthDieBonus,
        "wild-die": 6
    };

    if (existingActor === null && inData.wealthDieCalculated && inData.wealthDieCalculated.startsWith('d')) {
        wealth.die = parseInt(inData.wealthDieCalculated.split('d')[1]);
    }

    const details = {
        biography: {
            value: `<div class="swade-core">${descriptionHTML}${backgroundHTML}</div>`
        },
        species: {
            name: inData.race
        },
        wealth: wealth,
        autoCalcToughness: autoCalcToughness
    };

    const updateCurrency = existingActor && game.settings.get(MODULE_ID, 'updateCurrency');
    if (!existingActor || updateCurrency) {
        details.currency = inData.wealth;
    }

    if (game.settings.get(MODULE_ID, 'characterToughnessAutocalc')) {
        if (importMetadata.source !== '') {
            if (importMetadata.publisher !== '') {
                importMetadata.citation = `<p>Originally published in <cite>${importMetadata.source}</cite> by ${importMetadata.publisher}.<p>`;
            } else {
                importMetadata.citation = `<p>Originally published in <cite>${importMetadata.source}</cite>.<p>`;
            }
        }
        if (importMetadata.url === '') {
            importMetadata.url = 'https://savaged.us/';
        }
        if (importMetadata.createdDate === null) {
            importMetadata.createdDate = 'unknown';
        }
        if (importMetadata.updatedDate === null) {
            importMetadata.updatedDate = 'unknown';
        }
        if (game.settings.get(MODULE_ID, 'addImportData')) {
            details.biography.value = details.biography.value +
                `<footer class="savagedInfo">
                ${importMetadata.citation}
                <p>Imported from <a href="${importMetadata.url}">Savaged.us</a>!</p>
                <h4 class="bold">Import Details</h4>
                <ul>
                <li>uuid: <code>${importMetadata.uuid}</code></li>
                <li>Savaged.us version: <code>${importMetadata.appVersion}</code></li>
                <li>Importer: <a href="https://foundryvtt.com/packages/savagedus-importer/">Savaged.us Importer</a></li>
                <li>Created on Savaged.us: <code>${importMetadata.createdDate}</code></li>
                <li>Updated on Savaged.us: <code>${importMetadata.updatedDate}</code></li>
                <li>Imported on: <code>${importMetadata.importedDate()}</code></li>
                </ul>
                <p>Savaged.us and its logo are copyrighted by Alliante Entertainment.</p>
                <footer>`;
        }
    }

    return details;
}

function mapBennies(inData, inType) {
    // If Importing a Wild Card player character as an NPC, reduce the max Bennies by 1 since NPCs only get 2 Bennies.
    const exception = inData.bookName.includes('Pathfinder');
    const isNPC = inType === 'npc';
    let maxBennies = 3;
    if (isNPC && !exception) {
        maxBennies = 2;
    }
    let bennies = {
        max: maxBennies
    };
    return bennies;
}

function mapVehicularWeapons(inData) {
    let weapons = [];
    inData.weapons.forEach(item => {
        let name = 'Imported ' + item.name;
        let description = writeDescription(item);
        let dmgRoll = item.damage.replace(/Str/gi, '@str');
        let range = item.range;
        let isEquippable = true;
        let skill = '';
        let actions = {};
        if (range == "Melee") { // messes with the way SWADE detects ranged vs melee weapons ( blank range )
            range = '';
            skill = 'Fighting';
            actions.skill = skill;
        } else {
            skill = 'Shooting';
            actions.skill = skill;
        }
        weapons.push({
            name: name,
            type: "weapon",
            img: "systems/swade/assets/icons/weapon.svg",
            system: {
                description: description,
                weight: item.weight,
                notes: item.notes,
                quantity: item.quantity,
                price: item.cost,
                equippable: isEquippable,
                equipStatus: CONFIG.SWADE.CONST.EQUIP_STATE.EQUIPPED,
                isVehicular: true,
                range: range,
                damage: dmgRoll,
                rof: item.rof,
                ap: item.ap,
                shots: item.shots,
                minStr: item.minStr,
                armor: item.armor,
                attribute: item.attribute,
                actions: {
                    skill: skill,
                    skillMod: '',
                    dmgMod: ''
                }
            }
        });
    });
    return weapons;
}

async function mapVehicleItems(inData, existingVehicle, folderName, inType) {
    let items = [];

    if (inData.contains.gear) {
        items = mapVehicularWeapons(inData).concat(await mapItems(inData.contains, existingVehicle, folderName, inType));
    } else {
        items = mapVehicularWeapons(inData).concat(await mapItems(inData.contains, existingVehicle));
    }

    if (existingVehicle === null) {
        return items;
    }
}

//TODO: Map Skill based on vehicle type.
async function importVehicleData(inData, existingVehicle = null, folderName, options) {
    let ownerUUID = '';
    if (typeof options !== 'undefined' && typeof options.owningActor !== 'undefined' && typeof options.owningActor.uuid !== 'undefined') {
        ownerUUID = options.owningActor.uuid;
    }

    let wounds = 3;
    switch (inData.size) {
        case -4:
            inData.scale = -6;
            break;
        case -3:
            inData.scale = -4;
            break;
        case -2:
            inData.scale = -2;
            break;
        case -1:
        case 0:
        case 1:
        case 2:
        case 3:
            inData.scale = 0;
            break;
        case 4:
        case 5:
        case 6:
        case 7:
            inData.scale = 2;
            wounds = wounds + 1;
            break;
        case 8:
        case 9:
        case 10:
        case 11:
            inData.scale = 4;
            wounds = wounds + 2;
            break;
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
        case 18:
        case 19:
        case 20:
            inData.scale = 6;
            wounds = wounds + 3;
            break;
        default:
            inData.scale = 0;
    }
    let crew = inData.crew.split('+');
    let outData = {
        name: inData.name,
        img: 'icons/svg/target.svg',
        type: 'vehicle',
        sort: 100001,
        system: {
            size: inData.size,
            scale: inData.scale,
            classification: '',
            handling: inData.handling,
            cost: inData.cost,
            topspeed: inData.top_speed_mph,
            description: inData.notes,
            toughness: {
                total: inData.toughness,
                armor: inData.armor
            },
            wounds: {
                value: 0,
                max: wounds,
                ignored: 0
            },
            crew: {
                required: {
                    value: crew[0],
                    max: crew[0]
                },
                optional: {
                    value: crew[1],
                    max: crew[1]
                }
            },
            driver: {
                id: '',
                skill: '',
                skillAlternative: ''
            },
            status: {
                isOutOfControl: false,
                isWrecked: false
            },
            initiative: {
                hasHesitant: false,
                hasLevelHeaded: false,
                hasImpLevelHeaded: false
            },
            additionalStats: {},
            maxCargo: 0,
            maxMods: 0
        },
        items: await mapVehicleItems(inData, existingVehicle, folderName, 'vehicle'),
        flags: {
            [MODULE_ID]: {
                source: MODULE_TITLE,
                method: options.method,
                appVersion: inData.appVersion,
                created: inData.createdDate,
                updated: inData.updatedDate,
                uuid: inData.uuid,
                ownerUUID: ownerUUID
            }
        }
    };
    return outData;
}

export async function setDriverID(operatorActor) {
    let driverID = `Actor.${operatorActor.id}`,
        actors = game.actors;

    for (const actor of actors) {
        if (actor.system.type === 'vehicle' &&
            actor.getFlag(MODULE_ID, 'ownerUUID') === operatorActor.getFlag(MODULE_ID, 'uuid')
        ) {
            await actor. update({ 'system.driver.id': driverID });
        }
    }
}

async function getEquippedAs(item, type) {
    const equipped = item.equipped;
    const equippedAs = item.equippedAs;

    if (!equippedAs) {
        return CONFIG.SWADE.CONST.EQUIP_STATE.CARRIED;
    } else if (equipped) {
        if (type === 'gear') {
            return CONFIG.SWADE.CONST.EQUIP_STATE.EQUIPPED;
        } else if (type === 'weapon') {
            if (equippedAs === 'primary') {
                return CONFIG.SWADE.CONST.EQUIP_STATE.MAIN_HAND;
            } else if (equippedAs === 'secondary') {
                return CONFIG.SWADE.CONST.EQUIP_STATE.OFF_HAND;
            } else if (equippedAs === 'two-hands') {
                return CONFIG.SWADE.CONST.EQUIP_STATE.TWO_HANDS;
            }
        } else if (type === 'shield') {
            return CONFIG.SWADE.CONST.EQUIP_STATE.EQUIPPED;
        } else if (type === 'armor') {
            return CONFIG.SWADE.CONST.EQUIP_STATE.EQUIPPED;
        }
    } else {
        return CONFIG.SWADE.CONST.EQUIP_STATE.CARRIED;
    }
}

async function importDirectoryItem(item, importFolder, gearItemImport = false) {
    let subfolder = null,
        subfolderName = '',
        existingItem = game.items.find(existingItem => existingItem.name === item.name && existingItem.type === item.type && item.type !== 'ability');

    if (gearItemImport) {
        item.folder = importFolder.id;
        await Item.create(item);
    } else if (existingItem === undefined && item.type !== 'ability') {
        subfolderName = item.type;
        // Pluralize the subfolder name (e.g., Edges)
        if (item.type !== 'gear' && item.type !== 'armor' && item.type !== 'ability') {
            subfolderName = item.type + 's';
        }

        // Capitalize the first letter of the subfolder name (i.e., Edges vs. edges)
        subfolderName = subfolderName[0].toUpperCase() + subfolderName.substring(1);

        // Check if folder already exists.
        let subfolder = await game.folders.find((f) => f.type === 'Item' && f.parent === importFolder.id && f.name === subfolderName);
        // Check if the subfolder is null or if its name doesn't match the previous items subfolder name
        if (!subfolder) {
            // Create a new subfolder
            subfolder = await Folder.create({ name: subfolderName, type: 'Item', parent: importFolder.id });
        }
        item.folder = subfolder.id;
        await Item.create(item);
    }
}

async function importLanguages(inData) {
    let customLanguages = await game.settings.get('polyglot', 'customLanguages');
    if (customLanguages === '') {
        customLanguages = new Set();
    } else {
        customLanguages = new Set(customLanguages.split(',').map(function (lang) {
            return lang.trim();
        }));
    }

    const languagesKnownAbility = inData.abilities.find((a) => a.name === 'Languages Known');
    if (inData.languages.length) {
        for (const lang of inData.languages) {
            customLanguages = customLanguages.add(lang.name);
        }

    } else if (languagesKnownAbility && languagesKnownAbility.description) {
        const abilityLanguages = languagesKnownAbility.description.split(',').map(function (lang) {
            lang = lang.split(' (')[0];
            return lang.trim();
        });
        for (const lang of abilityLanguages) {
            customLanguages = customLanguages.add(lang);
        }
    }
    return Array.from(customLanguages).sort().join(',');
}