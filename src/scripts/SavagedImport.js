import { fetchSavagedusData } from "./api.js";
import { sortResults } from "./utils.js";
import { MODULE_ID } from "../savagedus-importer.js";

export default class SavagedImport extends Dialog {
    static results = [];
    activateListeners(html) {
        let searchString = '',
            importType = '',
            id = '';
        super.activateListeners(html);
        const searchButton = html.find("button.savagedus-button-search");
        const searchField = html.find("input.savagedus-search-string")[0];
        const resultsList = html.find("select.savagedus-results-list")[0];
        const loadingResults = html.find(".loading-spinner-results")[0];
        const importButton = html.find("button[data-button='Import']")[0];
        importButton.disabled = true;

        searchButton.on("click", async () => {
            resultsList.parentElement.style.display = 'none';

            const booksSelect = html.find("select.savagedus-book-select");
            let booksToSearch = [];
            let selectedOptions = booksSelect[0].selectedOptions;

            for (const option of selectedOptions) {
                booksToSearch.push(option.value);
            }

            const formClass = html.find(".savagedus-search-form");

            if (formClass.attr('class').includes('bestiary')) {
                importType = 'bestiary';
                id = 'id';
            } else if (formClass.attr('class').includes('gear')) {
                importType = document.getElementById('savagedus-type-select').selectedOptions[0].value;
                id = 'uuid';
            }

            this.clearSelect(resultsList);
            searchButton.disabled = true;
            loadingResults.style.display = 'inline-block';
            searchString = searchField.value;
            SavagedImport.results = await fetchSavagedusData(game.settings.get(MODULE_ID, "apiKey"), importType, { searchString: searchString, books: booksToSearch.join(`,`) });
            const sortedResults = await sortResults(SavagedImport.results);

            Object.keys(sortedResults).forEach(bookName => {
                let optionGroup = document.createElement("optgroup");
                let publisher = '';

                if (sortedResults[bookName][0].bookPublisher !== '') {
                    publisher = `${sortedResults[bookName][0].bookPublisher}`;
                }

                optionGroup.label = `${bookName} (${publisher})`;

                sortedResults[bookName].forEach(result => {
                    let option = document.createElement("option");
                    option.text = result.name;
                    option.value = result[id];
                    optionGroup.appendChild(option);
                });
                resultsList.parentElement.style.display = 'block';
                resultsList.appendChild(optionGroup);
                resultsList.parentElement.parentElement.parentElement.parentElement.parentElement.style.height = 'auto';
            });

            searchButton.disabled = false;
            loadingResults.style.display = 'none';
            importButton.disabled = false;
        });
    }

    clearSelect(selectElement) {
        let list = Array.from(selectElement.children);
        for (let child of list) {
            child.remove();
        }
    }
}