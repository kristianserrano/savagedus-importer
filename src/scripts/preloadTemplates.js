export const preloadTemplates = async function () {
    const templatePaths = [
        // Add paths to "modules/savagedus-importer/templates"
        'modules/savagedus-importer/templates/bestiary-search.hbs',
        'modules/savagedus-importer/templates/character-list.hbs',
        'modules/savagedus-importer/templates/file-import.hbs',
        'modules/savagedus-importer/templates/gear-search.hbs'
    ];

    return await loadTemplates(templatePaths);
};