# 3.8.3

## Fixed

-Fixes incorrect use of `.updateSource()`

If you find any additional bugs, please report them at https://gitlab.com/kristianserrano/savagedus-importer/-/issues
