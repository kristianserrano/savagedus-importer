# Savaged.us Importer for Foundry Virtual Tabletop

**This project has been sunset. As SWADE content modules have grown in their advanced uses of Active Effects, it's become nearly impossible to import data from Savaged.us in a way that won't conflict with versions of Items (Edges, Hindrances, Special Abilities, Races, etc.) that make use of Active Effects. A lot of the data in Savaged.us imports are written as fixed values, and when an Actor is patched with content that makes use of Active Effects, many values end up being applied twice, but it's difficult to track where and how. As such, the importer is effectively doing more harm than good and has been retired.**

*Note: Due to permissions related to Sidebar Directory Folders and file system directories, only users with Assistant or Gamemaster roles are able to use Savaged.us Importer.*

## Introduction

This module for Foundry Virtual Tabletop imports JSON files of *Savage Worlds Adventure Edition* characters exported from [Savaged.us](https://savaged.us/). To import directly from Savaged.us via its API, you will need a [Wildcard subscription to Savaged.us](https://savaged.us/wildcard-benefits/) which provides an API key.

This module requires the free, official *Savage Worlds Adventure Edition* system package for Foundry Virtual Tabletop.

[[_TOC_]]

## Features by Account Types

There are two levels of module functionality tied to wether or not you have a paid account at savaged.us

### Free, registered Savaged.us Accounts

- Import of generic JSON files exported from Savaged.us.
- Import Items owned by Actors into Items Directory.
- Import allied extras and vehicles included in the generic JSON file as Actors.

### Wildcard Savaged.us Account

As above plus direct import of the following using your [Savaged.us API key](https://savaged.us/me/api-key).

- Direct imports and updates of Savaged.us characters.
- Update imported characters directly from the character sheet.
- Direct import of bestiary entries via search or by book.

## Installation

You can find Savaged.us Importer through the built-in module manager available in the Foundry's Setup screen.

~or~

If you're using [The Forge](https://forge-vtt.com/), you can install the module via the [Bazaar](https://forge-vtt.com/bazaar#package=savagedus-importer).

~or~

- `git checkout https://gitlab.com/kristianserrano/savagedus-importer`
- `cd savagedus-importer`
- Edit your `foundryconfig.json` file to update to your data directory
- `npm install`
- `npm run build`
- `npm link` // This will create a link to this project in your foundry data directory

The module should now show up in your Foundry server and can be enabled in any SWADE games you have.

## Use

[Detailed Instructions with accompanying screenshots are availabe in the Wiki](https://gitlab.com/kristianserrano/savagedus-importer/-/wikis/How-to-Import-a-Character)

## Contributing

Thank you for being willing to contribute to Savaged.us Importer. Here are a few rules you should follow regarding a given issue, milestone, or merge request (MR).

First, please follow the general coding style and formatting, i.e., no semi-colons, 4 spaces for indendtation, etc.

Contribututions must come as merge requests. If there's a new bug you want to fix or a new feature you want to contribute, please make sure there is a corresponding issue created for it before submitting your MR. Be sure to include the issue # in the format of `Fixes #` in the MR; this will autoclose issues when they are successfully merged. The milestone in which the MR will be included will be assigned by the approvers.

Thanks!
